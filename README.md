#Hundred To One game
This is a simple web-based online variation of [Sto ko odnomu](https://en.wikipedia.org/wiki/Sto_k_odnomu) (aka [Family Feud](https://en.wikipedia.org/wiki/Family_Feud)).

##Testing
Use `mvn clean test -Parq-wildfly-embedded` or `mvn clean test -Parq-wildfly-managed`

##Running
Please, read [misc/configSteps.md](misc/configSteps.md)

##Notes
* All UI is currently in Russian
* In-game screenshots can be found in [misc/screenshots](misc/screenshots)
* The game doesn't support IE < 10
* The game was never tested on a linux server

