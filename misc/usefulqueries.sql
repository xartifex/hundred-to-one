--delete all game data
delete from synonym;
delete from playeranswer;
delete from playerstate;
delete from player;
delete from answer;
delete from question ;

--find the winner
select rownum () place,name, total from (
select sum(score) total, name from player, playeranswer where player = id group by id order by total desc);

--find the winner in Moscow
select rownum () place,name, total from (
select sum(score) total, name from player, playeranswer,
users where player = player.id and office='Moscow' and email=name group by player.id order by total desc);