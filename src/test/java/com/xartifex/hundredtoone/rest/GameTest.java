package com.xartifex.hundredtoone.rest;

import com.xartifex.hundredtoone.user.UserInfo;
import com.xartifex.hundredtoone.user.UserInfoProvider;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.xartifex.hundredtoone.data.GameDAO.INITIAL_LIVES;
import static com.xartifex.hundredtoone.rest.Game.SAME_ANSWER_ERROR;
import static com.xartifex.hundredtoone.rest.Game.SAME_MATCH_ERROR;
import static com.xartifex.hundredtoone.rest.TestDataCreator.*;
import static com.xartifex.hundredtoone.rest.TestUserInfoProvider.TEST_USER_EMAIL;
import static com.xartifex.hundredtoone.rest.TestUserInfoProvider.TEST_USER_NAME;


@RunWith(Arquillian.class)
public class GameTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap
                .create(WebArchive.class, "rest-test.war")
                .addPackage("com.xartifex.hundredtoone.rest")
                .addPackage("com.xartifex.hundredtoone.common")
                .addPackage("com.xartifex.hundredtoone.data")
                .addPackage("com.xartifex.hundredtoone.model")
                .addClass(UserInfo.class)
                .addClass(UserInfoProvider.class)
                .addAsResource("META-INF/test-persistence.xml",
                        "META-INF/persistence.xml")
                .addAsResource("import.sql")
                .addAsWebInfResource("WEB-INF/beans.xml", "beans.xml")
                .addAsWebInfResource("WEB-INF/hundred-to-one-test-ds.xml",
                        "hundred-to-one-test-ds.xml")
                .addAsWebInfResource("WEB-INF/jboss-web.xml", "jboss-web.xml")
                .addAsWebInfResource("WEB-INF/web.xml", "web.xml")
                .addAsLibraries(Maven.resolver().resolve("me.xdrop:fuzzywuzzy:1.1.8")
                        .withoutTransitivity().asSingleFile());
    }

    private String getBasicAuthentication() {
        String token = TEST_USER_EMAIL + ":1";
        try {
            return "BASIC " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Cannot encode with UTF-8", ex);
        }
    }

    Logger log = LoggerFactory.getLogger(GameTest.class);

    @Test
    @RunAsClient
    public void playGame(@ArquillianResteasyResource WebTarget webTarget) {
        //get first question
        Game game1 = makeTurn("", webTarget);
        Assert.assertEquals(QUESTION_1, game1.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, game1.getLives());
        Assert.assertEquals(TEST_USER_NAME, game1.getPlayerName());
        Assert.assertTrue(game1.getPlayerAnswers().size() == 0);

        //send correct answer
        Game game2 = makeTurn(ANSWER_1_1, webTarget);
        Assert.assertEquals(QUESTION_1, game2.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, game2.getLives());
        Assert.assertEquals(TEST_USER_NAME, game2.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game2.getGameState());
        List<ResultRow> expectedMatch2 = new ArrayList<>();
        expectedMatch2.add(new ResultRow(QUESTION_1, ANSWER_1_1, ANSWER_1_1_SCORE, ANSWER_1_1_POPULARITY));
        Assert.assertEquals(expectedMatch2, game2.getCurrentMatchedAnswers());
        Assert.assertTrue(game2.getPlayerAnswers().size() == 0);

        //wrong answer
        Game game3 = makeTurn(ANSWER_1_2, webTarget);
        Assert.assertEquals(QUESTION_1, game3.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES - 1, game3.getLives());
        Assert.assertEquals(TEST_USER_NAME, game3.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game3.getGameState());
        List<ResultRow> expectedMatch3 = new ArrayList<>();
        expectedMatch3.add(new ResultRow(QUESTION_1, ANSWER_1_1, ANSWER_1_1_SCORE, ANSWER_1_1_POPULARITY));
        Assert.assertEquals(expectedMatch3, game3.getCurrentMatchedAnswers());
        Assert.assertTrue(game3.getPlayerAnswers().size() == 0);

        //last correct answer for question 1, at this point next question should arrive
        Game game4 = makeTurn(ANSWER_1_3, webTarget);
        Assert.assertEquals(QUESTION_2, game4.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, game4.getLives());
        Assert.assertEquals(TEST_USER_NAME, game4.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game4.getGameState());
        //todo: artifex: how to handle case where last correct answer is hit: currently we will see
        //end of the game or next question and no info on last score
        //as a solution always return all correct answers
        Assert.assertTrue(game4.getCurrentMatchedAnswers().size() == 0);
        Assert.assertTrue(game4.getPlayerAnswers().size() == 0);

        Game game5 = makeTurn("wrong answer 1", webTarget);
        Assert.assertEquals(QUESTION_2, game5.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES - 1, game5.getLives());
        Assert.assertEquals(TEST_USER_NAME, game5.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game5.getGameState());
        Assert.assertTrue(game5.getCurrentMatchedAnswers().size() == 0);
        Assert.assertTrue(game5.getPlayerAnswers().size() == 0);

        Game game6 = makeTurn("wrong answer 2", webTarget);
        Assert.assertEquals(QUESTION_2, game6.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES - 2, game6.getLives());
        Assert.assertEquals(TEST_USER_NAME, game6.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game6.getGameState());
        Assert.assertTrue(game6.getCurrentMatchedAnswers().size() == 0);
        Assert.assertTrue(game6.getPlayerAnswers().size() == 0);

        //trying to answer with the same incorrect value
        Game game7 = makeTurn("wrong answer 2", webTarget);
        Assert.assertEquals(SAME_ANSWER_ERROR, game7.getKnownError());

        //giving correct answer
        Game game8 = makeTurn(ANSWER_2_1, webTarget);
        Assert.assertEquals(QUESTION_2, game8.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES - 2, game8.getLives());
        Assert.assertEquals(TEST_USER_NAME, game8.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game8.getGameState());
        Assert.assertThat(game8.getCurrentMatchedAnswers(), Matchers.containsInAnyOrder(
                new ResultRow(QUESTION_2, ANSWER_2_1, ANSWER_2_1_SCORE, ANSWER_2_1_POPULARITY)));
        Assert.assertTrue(game8.getPlayerAnswers().size() == 0);

        //the match already found, expecting an error
        Game game9 = makeTurn(ANSWER_2_1 + "t", webTarget);
        Assert.assertEquals(SAME_MATCH_ERROR, game9.getKnownError());

        //giving last correct answer, 1 question left
        Game game10 = makeTurn(ANSWER_2_2, webTarget);
        Assert.assertEquals(QUESTION_3, game10.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, game10.getLives());
        Assert.assertEquals(TEST_USER_NAME, game10.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game10.getGameState());
        Assert.assertTrue(game10.getPlayerAnswers().size() == 0);

        //giving first correct answer by its synonym
        //case shouldn't matter
        Game game11 = makeTurn(SYNONYM_3_1_1.toUpperCase(), webTarget);
        Assert.assertEquals(QUESTION_3, game11.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, game11.getLives());
        Assert.assertEquals(TEST_USER_NAME, game11.getPlayerName());
        Assert.assertEquals(GameState.PLAYING, game11.getGameState());
        Assert.assertTrue(game11.getPlayerAnswers().size() == 0);
        Assert.assertThat(game11.getCurrentMatchedAnswers(), Matchers.containsInAnyOrder(
                new ResultRow(QUESTION_3, ANSWER_3_1, ANSWER_3_1_SCORE, ANSWER_3_1_POPULARITY)));

        //using second synonym, but match already found, so error is expected
        Game game12 = makeTurn(SYNONYM_3_1_2, webTarget);
        Assert.assertEquals(SAME_MATCH_ERROR, game12.getKnownError());

        //using first synonym for second answer, no more correct answers and no more questions left
        //game over (question 4 is set to be ignored by the game)
        Game end = makeTurn(SYNONYM_3_2_1, webTarget);
        Assert.assertEquals("", end.getCurrentQuestion());
        Assert.assertEquals(INITIAL_LIVES, end.getLives());
        Assert.assertEquals(TEST_USER_NAME, end.getPlayerName());
        Assert.assertEquals(GameState.GAME_OVER, end.getGameState());
        Assert.assertThat(end.getCurrentMatchedAnswers(), Matchers.containsInAnyOrder(
                new ResultRow(QUESTION_3, ANSWER_3_1, ANSWER_3_1_SCORE, ANSWER_3_1_POPULARITY),
                new ResultRow(QUESTION_3, ANSWER_3_2, ANSWER_3_2_SCORE, ANSWER_3_2_POPULARITY)));
        Assert.assertThat(end.getPlayerAnswers(), Matchers.containsInAnyOrder(
                new ResultRow(QUESTION_1, ANSWER_1_1, ANSWER_1_1_SCORE, ANSWER_1_1_POPULARITY),
                new ResultRow(QUESTION_1, ANSWER_1_2, ANSWER_1_2_SCORE, ANSWER_1_2_POPULARITY),
                new ResultRow(QUESTION_1, ANSWER_1_3, ANSWER_1_3_SCORE, ANSWER_1_3_POPULARITY),
                new ResultRow(QUESTION_2, "wrong answer 1", 0, 0),
                new ResultRow(QUESTION_2, "wrong answer 2", 0, 0),
                new ResultRow(QUESTION_2, ANSWER_2_1, ANSWER_2_1_SCORE, ANSWER_2_1_POPULARITY),
                new ResultRow(QUESTION_2, ANSWER_2_2, ANSWER_2_2_SCORE, ANSWER_2_2_POPULARITY),
                new ResultRow(QUESTION_3, SYNONYM_3_1_1.toUpperCase(), ANSWER_3_1_SCORE, ANSWER_3_1_POPULARITY),
                new ResultRow(QUESTION_3, SYNONYM_3_2_1, ANSWER_3_2_SCORE, ANSWER_3_2_POPULARITY)
        ));
    }

    private Game makeTurn(String answer, WebTarget webTarget) {
        Response response = webTarget.path("/play")
                .request().header("Authorization", getBasicAuthentication())
                .buildPost(Entity.entity(new PlayRequest(answer)
                        , MediaType.APPLICATION_JSON)).invoke();
        return response.readEntity(Game.class);
    }


}