package com.xartifex.hundredtoone.rest;

import com.xartifex.hundredtoone.data.GameDAO;
import com.xartifex.hundredtoone.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.HashSet;
import java.util.Set;

//taken from here
// http://serviceorientedarchitect.com/how-to-seed-the-database-with-sample-data-for-an-arquillian-test/
@ApplicationScoped
public class TestDataCreator
{
    public static final String QUESTION_1 = "Most popular name?";
    //ANSWER_Q_A
    public static final String ANSWER_1_1 = "Smith";
    public static final int ANSWER_1_1_SCORE = 7;
    public static final int ANSWER_1_1_POPULARITY = 4;

    public static final String ANSWER_1_2 = "Boch147";
    public static final int ANSWER_1_2_SCORE = 0;
    public static final int ANSWER_1_2_POPULARITY = 0;

    public static final String ANSWER_1_3 = "Ivan";
    public static final int ANSWER_1_3_SCORE = 100;
    public static final int ANSWER_1_3_POPULARITY = 1;


    public static final String QUESTION_2 = "Foo?";

    public static final String ANSWER_2_1 = "bar";
    public static final int ANSWER_2_1_SCORE = 97;
    public static final int ANSWER_2_1_POPULARITY = 3;

    public static final String ANSWER_2_2 = "bar bar";
    public static final int ANSWER_2_2_SCORE = 42;
    public static final int ANSWER_2_2_POPULARITY = 6;

    public static final String QUESTION_3 = "Foo1?";
    public static final String ANSWER_3_1 = "bar";
    public static final int ANSWER_3_1_SCORE = 97;
    public static final int ANSWER_3_1_POPULARITY = 3;

    public static final String ANSWER_3_2 = "foo answer";
    public static final int ANSWER_3_2_SCORE = 42;
    public static final int ANSWER_3_2_POPULARITY = 4;

    public static final String SYNONYM_3_1_1 = "synonym";
    public static final String SYNONYM_3_1_2 = "abrakadabra";
    public static final String SYNONYM_3_2_1 = "something completely different";

    @PersistenceContext
    private EntityManager em;

    @Inject
    UserTransaction utx;


    @Inject
    GameDAO gameDAO;

    public void prepareDate(
            @Observes @Initialized(ApplicationScoped.class) final Object event) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        utx.begin();

        Question question1 = new Question();
        question1.setQuestion(QUESTION_1);

        Answer answer11 = new Answer();
        answer11.setQuestion(question1);
        answer11.setAnswer(ANSWER_1_1);
        answer11.setScore(ANSWER_1_1_SCORE);
        answer11.setPopularity(ANSWER_1_1_POPULARITY);

        Answer answer12 = new Answer();
        answer12.setQuestion(question1);
        answer12.setAnswer(ANSWER_1_3);
        answer12.setScore(ANSWER_1_3_SCORE);
        answer12.setPopularity(ANSWER_1_3_POPULARITY);

        Set<Answer> answers1 = new HashSet<>();
        answers1.add(answer11);
        answers1.add(answer12);

        question1.setAnswers(answers1);

        em.persist(question1);
        em.persist(answer11);
        em.persist(answer12);

        Question question2 = new Question();
        question2.setQuestion(QUESTION_2);

        Answer answer21 = new Answer();
        answer21.setQuestion(question2);
        answer21.setAnswer(ANSWER_2_1);
        answer21.setScore(ANSWER_2_1_SCORE);
        answer21.setPopularity(ANSWER_2_1_POPULARITY);

        Answer answer22 = new Answer();
        answer22.setQuestion(question2);
        answer22.setAnswer(ANSWER_2_2);
        answer22.setScore(ANSWER_2_2_SCORE);
        answer22.setPopularity(ANSWER_2_2_POPULARITY);

        Set<Answer> answers2 = new HashSet<>();
        answers2.add(answer21);
        answers2.add(answer22);

        question2.setAnswers(answers2);
        em.persist(question2);
        em.persist(answer21);
        em.persist(answer22);

        Question question3 = new Question();
        question3.setQuestion(QUESTION_3);

        Answer answer31 = new Answer();
        answer31.setQuestion(question3);
        answer31.setAnswer(ANSWER_3_1);
        answer31.setScore(ANSWER_3_1_SCORE);
        answer31.setPopularity(ANSWER_3_1_POPULARITY);

        Set<Synonym> synonyms31 = new HashSet<>();
        Synonym synonym311 = new Synonym();
        synonym311.setSynonym(SYNONYM_3_1_1);
        synonym311.setAnswer(answer31);
        synonyms31.add(synonym311);
        Synonym synonym312 = new Synonym();
        synonym312.setSynonym(SYNONYM_3_1_2);
        synonym312.setAnswer(answer31);
        synonyms31.add(synonym312);
        answer31.setSynonyms(synonyms31);

        Answer answer32 = new Answer();
        answer32.setQuestion(question3);
        answer32.setAnswer(ANSWER_3_2);
        answer32.setScore(ANSWER_3_2_SCORE);
        answer32.setPopularity(ANSWER_3_2_POPULARITY);

        Set<Synonym> synonyms32 = new HashSet<>();
        Synonym synonym321 = new Synonym();
        synonym321.setSynonym(SYNONYM_3_2_1);
        synonym321.setAnswer(answer32);
        synonyms32.add(synonym321);
        answer32.setSynonyms(synonyms32);

        Set<Answer> answers3 = new HashSet<>();
        answers3.add(answer31);
        answers3.add(answer32);

        question3.setAnswers(answers3);
        em.persist(question3);
        em.persist(answer31);
        em.persist(synonym311);
        em.persist(synonym312);
        em.persist(answer32);
        em.persist(synonym321);

        Question question4 = new Question();
        question4.setQuestion("Ignored question");
        question4.setIncluded(false);

        Answer answer14 = new Answer();
        answer14.setQuestion(question4);
        answer14.setAnswer("ignored answer");
        answer14.setScore(150);
        answer14.setPopularity(2);

        Set<Answer> answers4 = new HashSet<>();
        answers4.add(answer14);
        question1.setAnswers(answers4);

        em.persist(question4);
        em.persist(answer14);


        utx.commit();
    }
}