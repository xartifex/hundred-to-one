/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xartifex.hundredtoone.model;


import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import java.util.HashSet;
import java.util.Set;


@RunWith(Arquillian.class)
public class ModelTest {
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap
                .create(WebArchive.class, "model-test.war")
                .addPackage("com.xartifex.hundredtoone.model")
                .addPackage("com.xartifex.hundredtoone.common")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("META-INF/log4j.properties", "classes/log4j.properties")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource("arquillian-ds.xml");
    }

    @PersistenceContext
    EntityManager entityManager;

    @Inject
    UserTransaction utx;

    @Inject
    Logger log;

    @Test
    @InSequence(1)
    public void testCreatePlayer() throws Exception {
        Question question = new Question();
        question.setQuestion("What is the meaning of life?");
        Player player = new Player();
        player.setName("TestPlayer");
        player.setLives(3);
        player.setCurrentQuestion(question);
        entityManager.persist(question);
        entityManager.persist(player);
        log.info("Question created " + player);
        log.info("Player created " + player);
    }

    @Test
    @InSequence(2)
    public void testCreateAnswer() throws Exception {
        Question question = new Question();
        question.setQuestion("What?");
        Answer answer = new Answer();
        answer.setQuestion(question);
        answer.setAnswer("Nothing!");
        answer.setScore(100);
        answer.setPopularity(5);

        Set<Answer> answers = new HashSet<>();
        answers.add(answer);
        question.setAnswers(answers);

        entityManager.persist(question);
        entityManager.persist(answer);
        Question fromDb = entityManager.find(Question.class, question.getQuestionId());
        log.info("found in db " + fromDb);
        log.info("found in db " + fromDb.getAnswers());
        log.info("Question created " + question);
        log.info("Answer created " + answer);
    }

    @Test
    @InSequence(3)
    public void testLoadQuestion() throws Exception {
        Question fromDb = entityManager.find(Question.class, 3L);
        log.info("found in db " + fromDb);
        log.info("found in db " + fromDb.getAnswers());
    }

    @Test
    @InSequence(4)
    public void testSimulateGameTurn() throws Exception {
        Player player = new Player();
        player.setName("Test Player 1");

        Question question = new Question();
        question.setQuestion("How would you name a dog?");

        Answer answer1 = new Answer();
        answer1.setQuestion(question);
        answer1.setAnswer("Jack Sparrow");
        answer1.setScore(7);
        answer1.setPopularity(4);

        Answer answer2 = new Answer();
        answer2.setQuestion(question);
        answer2.setAnswer("Snoop");
        answer2.setScore(70);
        answer2.setPopularity(1);

        Set<Answer> answers = new HashSet<>();
        answers.add(answer1);
        answers.add(answer2);

        question.setAnswers(answers);

        player.setCurrentQuestion(question);

        //player answers a question successfully:
        PlayerAnswer playerAnswer = new PlayerAnswer();
        playerAnswer.setPlayer(player);
        playerAnswer.setQuestion(question);
        playerAnswer.setAnswer("Snoop");
        playerAnswer.setMatch(answer2);
        playerAnswer.setScore(answer2.getScore());

        PlayerStatePK playerStatePK = new PlayerStatePK();
        playerStatePK.setPlayer(player);
        playerStatePK.setQuestion(question);
        PlayerState playersState = new PlayerState();
        playersState.setPlayerStatePK(playerStatePK);
        playersState.setLives(3);

        entityManager.persist(question);
        entityManager.persist(player);
        entityManager.persist(answer1);
        entityManager.persist(answer2);
        entityManager.persist(playerAnswer);
        entityManager.persist(playersState);
    }

    @Before
    public void setUp() throws SystemException, NotSupportedException {
        utx.begin();
    }

    @After
    public void commitTransaction() throws Exception {
        utx.commit();
    }

}
