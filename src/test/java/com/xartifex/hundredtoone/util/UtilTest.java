package com.xartifex.hundredtoone.util;

import com.xartifex.hundredtoone.model.Answer;
import com.xartifex.hundredtoone.model.Question;
import com.xartifex.hundredtoone.user.UserInfo;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class UtilTest {

    public static final String TEST_INPUT_1 ="how long should i________?\n" +
            "nap, 10000\n" +
            "pump for, 9000\n" +
            "bake chicken breast, 8000\n" +
            "sleep, 7000\n" +
            "boil eggs/ fry eggs/ cook eggs, 6000\n" +
            "study for the gre, 5000\n";

    public static final String TEST_INPUT_2="Пoчeмy нe пoкaзывaeт тeлeвизop?,\n" +
            "Сломался / поломался / разбился, 60\n" +
            "Нет света, 50\n" +
            "Нет сигнала, 40\n" +
            "Сгорел, 30\n" +
            "Плохая погода, 20\n" +
            "Не заплатили, 10\n" +
            "@\n" +
            "Пoчeмy yчитeль нe пoшёл в шкoлy?,\n" +
            "Зaбoлeл, 60\n" +
            "Пpocпaл, 50\n" +
            "Bыxoднoй, 40\n" +
            "Kaникyлы, 30\n" +
            "Нe зaxoтeл, 20\n" +
            "Oтпycк, 10";

    public static final String USERS_TEST_INPUT =
            "Ivan Ivanov, Иванов Иван Иванович, ivan.ivanov@email.com\n" +
            "Petr Petrov, Петров Петр Петрович, petr.petrov@email.com\n" +
            "Sidor Sidorov, Сидоров Сидор Сидорович, sidor.sidorov@email.com\n" +
            "Some Some,,some.some@email.com\n";

    public static final Question EXPECTED_QUESTION_FOR_INPUT_1 = new Question();
    {
        Set<Answer> answers = new HashSet<>();
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "nap", 10000, 1));
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "pump for", 9000, 2));
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "bake chicken breast", 8000, 3));
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "sleep", 7000, 4));
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "boil eggs", 6000, 5));
        answers.add(Answer.createAnswer(EXPECTED_QUESTION_FOR_INPUT_1, "study for the gre", 5000, 6));
        EXPECTED_QUESTION_FOR_INPUT_1.setAnswers(answers);
    }


    @Test
    public void getQuestion() throws Exception {
        Assert.assertEquals(EXPECTED_QUESTION_FOR_INPUT_1, Util.getQuestion(TEST_INPUT_1));
    }

    @Test
    public void getQuestions() throws Exception {
        //for now no error is a pass
        Util.getQuestions(TEST_INPUT_2);
    }

    @Test
    public void getUsers() throws Exception {
        Set<UserInfo> users = Util.getUsers(USERS_TEST_INPUT);
        Assert.assertTrue(users.size() == 4);
    }
}