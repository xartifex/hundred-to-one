var token=Cookies.get("token");

function conditionalBlink(elem, newValue) {
     if(elem.text() != newValue) {
        elem.fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
     }
}

var colorState = 0;
function changeColor(elem, newValue) {
    if(elem.text() != newValue) {
           switch(colorState) {
               case 0:
                   elem.css("color", "#0087ff");
                   break;
               case 1:
                   elem.css("color", "#ff6c00");
                   break;
               case 2:
                   elem.css("color", "#4325dc");
                   break;
               default:
           }
           colorState=colorState+1;
    }
}

get = function(obj, key) {
    return key.split(".").reduce(function(o, x) {
        return (typeof o == "undefined" || o === null) ? o : o[x];
    }, obj);
}

function play() {
    token=Cookies.get("token");
    var headers = {}
    if (token) {
        headers = { "Authorization":"Basic " + token }
    } else {

    }
    $("#main").removeAttr("style");
    $("#main").show();

    //fetch answer
    var answer = $('#answer').val();

    $.ajax({
        url: 'rest/play',
        contentType: "application/json",
        dataType: "json",
        headers: headers,
        type: "POST",
        data: JSON.stringify({"answer" : answer}),
        success: function(data) {
            var game = data;
            if(game.gameState=="GAME_OVER") {
                 $("#send").hide();
                 $("#matched").hide();
                 $("#stats").hide();
                 $("#tableName").hide();
                 $("#instruction").hide();
                 //unhide
                 $("#finish").text("Игра окончена, " + game.playerName + "!").removeAttr("style");
                 $("#finish").removeAttr("style");
                 $("#resultsmsg").removeAttr("style");
            }
            else {
                //todo: refactor this
                $("#matched").replaceWith('<table class="table table-bordered" id="matched">\
                                                   <tr>\
                                                       <td id="1">1 место</td>\
                                                       <td id="4">4 место</td>\
                                                   </tr>\
                                                   <tr>\
                                                       <td id="2">2 место</td>\
                                                       <td id="5">5 место</td>\
                                                   </tr>\
                                                   <tr>\
                                                       <td id="3">3 место</td>\
                                                       <td id="6">6 место</td>\
                                                   </tr>\
                                                   </table>');
                var q = $("#question");
                conditionalBlink(q, game.currentQuestion);
                changeColor(q, game.currentQuestion);
                q.text(game.currentQuestion);
                $("#player").text(game.playerName);
                var livesLeft = game.lives;
                var l = $("#lives");
                conditionalBlink(l, livesLeft);
                drawNoMatch(l, livesLeft);
                l.text(livesLeft).css("color",
                    livesLeft == 3 ? "green": (livesLeft == 2 ? "#FF8C00" : "red"));
                var matchedAnswers = game.currentMatchedAnswers;
                for (var i=0; i < matchedAnswers.length; i++){
                    var answer = matchedAnswers[i];
                    $("#" + answer.popularity).text(answer.popularity + " место: " + answer.answer + " - " + answer.score + " баллов");
                }
                $("#currentScore").text(game.currentScore);
            }
            var playerAnswers = game.playerAnswers;
            if(playerAnswers.length > 0) {
                var content = "<tbody><tr><th>вопрос</th><th>ответ</th><th>очки</th><th>№ строки по популярности</th></tr>";
                var totalScore = 0;
                for(var i =0; i < playerAnswers.length; i++){
                    var answer = playerAnswers[i];
                    content += "<tr>";
                    content += "<td>" + answer.question + "</td>";
                    content += "<td>" + answer.answer + "</td>";
                    content += "<td>" + answer.score + "</td>";
                    content += "<td>" + answer.popularity + "</td>";
                    content += "</tr>";
                    totalScore += answer.score;
                }
                content +=  "<tfoot><tr><td>Сумма</td><td></td><td>";
                content +=  totalScore + "</td><td></td></tr></tfoot>";

                $("#results").removeAttr("style");
                $("#results").html(content);
            }
        },
        error: function(error) {
           if(error.status == 401 || error.status == 403)
           {
               $("#main").hide();
               $("#auth").show();
               $("#loginError").text("Ошибка! Проверьте написание вашей рабочей почты!");
               Cookies.remove("token");
               token = null;
           }
           //todo: handle case when responseJSON is undefined
           var knownError = get(error, "responseJSON.knownError");
           if(knownError) {
                if(knownError == "UNIQUE_Q_P_A") {
                    knownError = "Вы уже так отвечали!";
                } else if(knownError == "UNIQUE_Q_P_M") {
                    knownError = "Этот ответ совпадает с одним из найденных Вами ранее!";
                }
                $("#alert").hide();
                $("#alert-text").text(knownError);
                $("#alert").fadeTo(2000, 1000).slideUp(1000, function(){
                    $("#alert").slideUp(1000);
                });
           } else {
                console.log("Error");
                console.log(error);
           }
        },
        complete: function() {
            $("#send").trigger('reset');
            $("#answer").focus();
        }
    });
}

function addQuestions() {
   var questionsString = $('#data').val();
   $.ajax({
       type: "POST",
       url: "rest/admin/addQuestions",
       data: questionsString,
       contentType: 'text/plain'
   })
}

function addPlayers() {
   var playersData = $('#playersData').val();
   $.ajax({
       type: "POST",
       url: "rest/admin/addPlayers",
       data: playersData,
       contentType: 'text/plain'
   })
}

function _drawNoMatch() {
    var canvas = document.getElementById("noMatch");
    var ctx = canvas.getContext("2d");
    ctx.lineWidth=10;
    ctx.strokeStyle="#FF0000";
    ctx.strokeRect(10, 10, 300, 300);

    ctx.beginPath();
    ctx.moveTo(10,10);
    ctx.lineTo(310,310);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(10,310);
    ctx.lineTo(310,10);
    ctx.stroke();
}

function drawNoMatch(livesElement, newValue) {
     if(livesElement.text() != newValue) {
            _drawNoMatch();
            $("#noMatch").show().fadeOut("slow");
     }
}

//taken from https://stackoverflow.com/a/15983064
function isIE () {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
/*
if (isIE () && isIE () < 9) {
 // is IE version less than 9
} else {
 // is IE 9 and later or not IE
}
*/
//todo: print alert if IE older than 10 version is used

//wow - such encryption - much security - so user - many hacks
function authenticate() {
    var storedToken=Cookies.get("token");
    if(storedToken) {
         $("#auth").hide();
         $("#main").removeAttr("style");
         token=storedToken;
         play();
         return;
    }
    $("#loginError").hide();
    var nameSurname = $('#inputEmail').val();
    if(nameSurname) {
        var token = btoa(nameSurname.toLowerCase() + ':1');
        $.ajax({
            url: 'rest/play',
            contentType: "application/json",
            dataType: "json",
            headers: {
                    "Authorization":'Basic ' + token
            },
            type: "POST",
            data: JSON.stringify({"answer" : ""}),
            success: function(data) {
                Cookies.set("token", token);
                $("#auth").hide();
                play();
            },
            error: function(error) {
                $("#loginError").removeAttr("style");
                $("#loginError").css("color","red");
                if(error.status == 401 || error.status == 403)
                {
                    $("#loginError").text("Ошибка! Проверьте написание вашей рабочей почты!");
                }
                else {
                    $("#loginError").text("Что-то пошло не так!");
                }
            },
            complete: function() {
            }
        });
    }
    else {
         //do nothing
    }
}