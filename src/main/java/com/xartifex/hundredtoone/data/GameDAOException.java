package com.xartifex.hundredtoone.data;

public class GameDAOException extends Exception {
    public GameDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
