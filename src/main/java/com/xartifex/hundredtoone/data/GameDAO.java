package com.xartifex.hundredtoone.data;

import com.xartifex.hundredtoone.model.*;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@ApplicationScoped
public class GameDAO {

    public static int INITIAL_LIVES = 3;

    @Inject
    private EntityManager em;

    public Player findById(Long id) {
        return em.find(Player.class, id);
    }

    public Player findByName(String name) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Player> criteria = cb.createQuery(Player.class);
            Root<Player> player = criteria.from(Player.class);
            criteria.select(player).where(cb.equal(player.get("name"), name));
            return em.createQuery(criteria).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Question getQuestionForNewPlayer() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Question> criteria = cb.createQuery(Question.class);
        Root<Question> question = criteria.from(Question.class);
        criteria.select(question).where(cb.equal(question.get("isIncluded"), true));
        return em.createQuery(criteria).setMaxResults(1).getSingleResult();
    }

    public Question getNextQuestion(Player player) {
        //todo: rewrite this using Criteria API as a single query or at least add caching
        CriteriaBuilder cb1 = em.getCriteriaBuilder();
        CriteriaQuery<PlayerState> cq1 = cb1.createQuery(PlayerState.class);
        Root<PlayerState> playersStates = cq1.from(PlayerState.class);
        cq1.select(playersStates).where(cb1.equal(playersStates.get("playerStatePK").get("player"), player));
        Set<PlayerState> userStates = new HashSet<>(em.createQuery(cq1).getResultList());
        //collect answered questions
        Set<Question> answered = new HashSet<>();
        for (PlayerState playerState : userStates) {
            answered.add(playerState.getPlayerStatePK().getQuestion());
        }

        CriteriaBuilder cb2 = em.getCriteriaBuilder();
        CriteriaQuery<Question> cq2 = cb2.createQuery(Question.class);
        Root<Question> question = cq2.from(Question.class);
        cq2.select(question).where(cb2.equal(question.get("isIncluded"), true));
        TypedQuery<Question> q = em.createQuery(cq2);
        Set<Question> allQuestions = new HashSet<>(q.getResultList());

        //it's going to work if we update current question only when user is out of lives
        allQuestions.removeAll(answered);
        if (allQuestions.isEmpty()) {
            return null;
        } else {
            return allQuestions.iterator().next();
        }
    }

    public Set<PlayerAnswer> getPlayerAnswers(Player player) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PlayerAnswer> cq = cb.createQuery(PlayerAnswer.class);
        Root<PlayerAnswer> answer = cq.from(PlayerAnswer.class);
        cq.select(answer).where(cb.equal(answer.get("player"), player));
        TypedQuery<PlayerAnswer> a = em.createQuery(cq);
        return new LinkedHashSet<>(a.getResultList());
    }

    //note: definitely not the most optimal way to do it
    public int getCurrentScore(Player player) {
        int score = 0;
        for (PlayerAnswer playerAnswer : getPlayerAnswers(player)) {
            score += playerAnswer.getScore();
        }
        return score;
    }

    public Set<PlayerAnswer> getPlayerCurrentAnswers(Player player) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PlayerAnswer> cq = cb.createQuery(PlayerAnswer.class);
        Root<PlayerAnswer> answer = cq.from(PlayerAnswer.class);
        cq.select(answer).where(cb.and(cb.equal(answer.get("player"), player),
                cb.equal(answer.get("question"), player.getCurrentQuestion())));
        TypedQuery<PlayerAnswer> a = em.createQuery(cq);
        return new HashSet<>(a.getResultList());
    }

    public Set<Answer> getCurrentMatchedAnswers(Player player) {
        //todo: rewrite this using Criteria API as a single query or at least add caching
        Set<Answer> matchedAnswers = new HashSet<>();
        Set<PlayerAnswer> playerAnswers = getPlayerCurrentAnswers(player);
        for (PlayerAnswer playerAnswer : playerAnswers) {
            Answer answer = playerAnswer.getMatch();
            if (answer != null) {
                matchedAnswers.add(answer);
            }
        }
        return matchedAnswers;
    }

    public Player addNewPlayer(String name) {
        Player player = new Player();
        player.setName(name);
        player.setLives(INITIAL_LIVES);
        Question question = getQuestionForNewPlayer();
        player.setCurrentQuestion(question);
        em.persist(player);
        return player;
    }

    public void addQuestion(Question question) {
        em.persist(question);
        for (Answer answer : question.getAnswers()) {
            em.persist(answer);
            for (Synonym synonym : answer.getSynonyms()) {
                em.persist(synonym);
            }
        }
    }

    public void addQuestions(Collection<Question> questions) {
        for (Question question : questions) {
            addQuestion(question);
        }
    }

    public void updatePlayersState(PlayerState playersState) {
        em.merge(playersState);
    }

    public void addPlayersAnswer(PlayerAnswer playerAnswer) {
        em.persist(playerAnswer);
    }
}
