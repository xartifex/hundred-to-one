package com.xartifex.hundredtoone.user;

public interface UserInfoProvider {
    public UserInfo getUserInfoByEmail(String email);
}
