package com.xartifex.hundredtoone.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class PlayerStatePK implements Serializable {
    @ManyToOne
    @JoinColumn
    private Question question;

    @ManyToOne
    @JoinColumn
    private Player player;


    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "PlayerStatePK{" +
                "question=" + question +
                ", player=" + player +
                '}';
    }
}