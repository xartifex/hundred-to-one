package com.xartifex.hundredtoone.model;

public enum GameState {
    NORMAL,
    GAME_OVER
}
