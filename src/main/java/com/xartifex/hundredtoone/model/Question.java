package com.xartifex.hundredtoone.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"question"}, name = "UNIQUE_QUESTION"))
public class Question implements Serializable {
    @Id
    @GeneratedValue
    private long questionId;

    @NotNull
    @Size(min = 1, max = 3000, message = "1-3000 letters and spaces")
    private String question;

    @OneToMany(mappedBy = "question")
    @NotNull
    private Set<Answer> answers = new HashSet<>();

    private boolean isIncluded = true;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    //todo: compare by questions string and at least log a warning if two questions with dif ids have the same
    //string question or vise versa
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        return questionId == question.questionId;
    }

    public boolean isIncluded() {
        return isIncluded;
    }

    public void setIncluded(boolean included) {
        isIncluded = included;
    }

    @Override
    public int hashCode() {
        return (int) (questionId ^ (questionId >>> 32));
    }

    @Override
    public String toString() {
        return "Question{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", answers=" + answers +
                ", isIncluded=" + isIncluded +
                '}';
    }
}
