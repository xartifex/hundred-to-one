package com.xartifex.hundredtoone.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"synonym", "answer"},
        name="UNIQUE_S_A")})
public class Synonym implements Serializable{

    @GeneratedValue
    @Id
    private long id;

    @ManyToOne
    @JoinColumn(name="answer")
    @NotNull
    private Answer answer;

    @NotNull
    @Size(min = 1, max = 3000, message = "1-3000 letters and spaces")
    @Column(name="synonym")
    private String synonym;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public String getSynonym() {
        return synonym;
    }

    public void setSynonym(String synonym) {
        this.synonym = synonym;
    }

    @Override
    public String toString() {
        return "Synonym{" +
                "id=" + id +
                ", answerId=" + answer.getAnswerId() +
                ", synonym='" + synonym + '\'' +
                '}';
    }
}
