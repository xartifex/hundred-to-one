package com.xartifex.hundredtoone.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
public class Player implements Serializable {
    //todo: make primary key composite: id, name
    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Size(min = 1, max = 200, message = "1-200 letters and spaces")
    private String name;

    @NotNull
    @ManyToOne
    private Question currentQuestion;

    private int lives;

    private GameState gameState = GameState.NORMAL;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(Question currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currentQuestion=" + currentQuestion +
                ", lives=" + lives +
                ", gameState=" + gameState +
                '}';
    }
}
