package com.xartifex.hundredtoone.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"question", "answer"}, name = "UNIQUE_QUESTION_ANSWER"))
public class Answer implements Serializable {
    @Id
    @GeneratedValue
    private long answerId;

    @ManyToOne
    @JoinColumn(name = "question")
    @NotNull
    private Question question;

    @NotNull
    @Column(name = "answer")
    @Size(min = 1, max = 3000, message = "1-3000 letters and spaces")
    private String answer;

    @Min(0)
    private int score;

    @Min(1)
    @Max(6)
    private int popularity;

    @OneToMany(mappedBy = "answer")
    @NotNull
    private Set<Synonym> synonyms = new HashSet<Synonym>();

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer1 = (Answer) o;

        if (question != null ? !question.equals(answer1.question) : answer1.question != null) return false;
        return answer != null ? answer.equals(answer1.answer) : answer1.answer == null;
    }

    @Override
    public int hashCode() {
        int result = question != null ? question.hashCode() : 0;
        result = 31 * result + (answer != null ? answer.hashCode() : 0);
        return result;
    }

    public Set<Synonym> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(Set<Synonym> synonyms) {
        this.synonyms = synonyms;
    }

    public static Answer createAnswer(Question question, String answer, int score, int popularity) {
        Answer answerEntity = new Answer();
        answerEntity.setQuestion(question);
        answerEntity.setAnswer(answer);
        answerEntity.setScore(score);
        answerEntity.setPopularity(popularity);
        return answerEntity;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "answerId=" + answerId +
                ", questionId=" + question.getQuestionId() +
                ", answer='" + answer + '\'' +
                ", score=" + score +
                ", popularity=" + popularity +
                ", synonyms=" + synonyms +
                '}';
    }
}
