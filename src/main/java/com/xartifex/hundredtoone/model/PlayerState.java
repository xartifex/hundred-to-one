package com.xartifex.hundredtoone.model;

import javax.persistence.*;
import javax.validation.constraints.Min;


@Entity
public class PlayerState {
    @EmbeddedId
    PlayerStatePK playerStatePK;

    @Min(0)
    private int lives;


    public PlayerStatePK getPlayerStatePK() {
        return playerStatePK;
    }

    public void setPlayerStatePK(PlayerStatePK playerStatePK) {
        this.playerStatePK = playerStatePK;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    @Override
    public String toString() {
        return "PlayerState{" +
                "playerStatePK=" + playerStatePK +
                ", lives=" + lives +
                '}';
    }
}
