package com.xartifex.hundredtoone.model;

import javax.persistence.*;
import javax.validation.Constraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"question", "player", "match"}, name="UNIQUE_Q_P_M"),
@UniqueConstraint(columnNames = {"question", "player", "answer"}, name = "UNIQUE_Q_P_A")})
public class PlayerAnswer implements Serializable {

    @Id
    @GeneratedValue
    private long playersAnswerId;

    @JoinColumn(name = "question")
    @ManyToOne
    @NotNull
    private Question question;

    @JoinColumn(name = "player")
    @ManyToOne
    @NotNull
    private Player player;

    @Size(min = 1, max = 200, message = "1-200 letters and spaces")
    private String answer;

    @JoinColumn(name = "match")
    @ManyToOne
    //can be null
    private Answer match;

    @Min(0)
    private int score;

    public long getPlayersAnswerId() {
        return playersAnswerId;
    }

    public void setPlayersAnswerId(long playersAnswerId) {
        this.playersAnswerId = playersAnswerId;
    }


    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Answer getMatch() {
        return match;
    }

    public void setMatch(Answer match) {
        this.match = match;
    }

    @Override
    public String toString() {
        return "PlayerAnswer{" +
                "playersAnswerId=" + playersAnswerId +
                ", question=" + question +
                ", player=" + player +
                ", answer='" + answer + '\'' +
                ", match=" + match +
                ", score=" + score +
                '}';
    }
}
