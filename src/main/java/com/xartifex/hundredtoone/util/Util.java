package com.xartifex.hundredtoone.util;

import com.xartifex.hundredtoone.model.Answer;
import com.xartifex.hundredtoone.model.Question;
import com.xartifex.hundredtoone.model.Synonym;
import com.xartifex.hundredtoone.user.UserInfo;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

//Note: String is not Closeable so there is no need in calling scanner.close();
public class Util {
    public static Question getQuestion(String data) {
        Scanner newLineScanner = new Scanner(data);
        String line = newLineScanner.nextLine();
        //1 - question itself
        Scanner scanner = new Scanner(line);
        scanner.useDelimiter(",");
        String question = scanner.next();
        List<Pair<List<String>, Pair<Integer, Integer>>> answerToScoreAndPopularityPairs = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            String answerStr = newLineScanner.nextLine();
            answerStr = answerStr.trim();
            Scanner commaVal = new Scanner(answerStr);
            commaVal.useDelimiter(",");
            String answer = commaVal.next();
            Scanner synonymsScan = new Scanner(answer);
            synonymsScan.useDelimiter("/");
            List<String> foundSynonyms = new ArrayList<>();
            while (synonymsScan.hasNext()) {
                String s = synonymsScan.next();
                foundSynonyms.add(s);
            }
            int score = Integer.parseInt(commaVal.next().trim());
            Pair<Integer, Integer> scorePopularity = new ImmutablePair<>(score, i);
            Pair<List<String>, Pair<Integer, Integer>> allAnswersToScoreAndPopularity = new ImmutablePair<>(foundSynonyms, scorePopularity);
            answerToScoreAndPopularityPairs.add(allAnswersToScoreAndPopularity);
        }
        Question questionEntity = new Question();
        questionEntity.setQuestion(question.trim());

        Set<Answer> answers = new HashSet<>();
        for (Pair<List<String>, Pair<Integer, Integer>> pair : answerToScoreAndPopularityPairs) {
            Answer answer = new Answer();
            List<String> synonyms = pair.getKey();
            String strAnswer = synonyms.get(0).trim();
            answer.setAnswer(strAnswer);
            Set<Synonym> synonymsSet = new HashSet<>();
            for (int i = 1; i < synonyms.size(); i++) {
                Synonym synonym = new Synonym();
                synonym.setAnswer(answer);
                synonym.setSynonym(synonyms.get(i).trim());
                synonymsSet.add(synonym);
            }
            answer.setSynonyms(synonymsSet);
            Integer score = pair.getValue().getKey();
            Integer popularity = pair.getValue().getValue();
            answer.setScore(score);
            answer.setPopularity(popularity);
            answer.setQuestion(questionEntity);
            answers.add(answer);
        }
        questionEntity.setAnswers(answers);
        return questionEntity;
    }

    public static List<Question> getQuestions(String data) {
        Scanner scanner = new Scanner(data);
        scanner.useDelimiter("@\n");
        List<Question> questions = new ArrayList<>();
        while (scanner.hasNext()) {
            String q = scanner.next();
            questions.add(getQuestion(q));
        }
        return questions;
    }

    public static Set<UserInfo> getUsers(String data) {
        Set<UserInfo> users = new HashSet<>();
        Scanner scanner = new Scanner(data);
        while (scanner.hasNextLine()) {
            Scanner csv = new Scanner(scanner.nextLine());
            csv.useDelimiter(",");
            String name = csv.next();
            String rusName = csv.next();
            String email = csv.next();
            String office = csv.hasNext() ? csv.next() : "";
            users.add(new UserInfo(name.trim(), rusName.trim(), email.trim().toLowerCase(), office.trim()));
        }
        return users;
    }
}
