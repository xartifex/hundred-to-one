package com.xartifex.hundredtoone.rest;

public enum GameState {
    PLAYING,
    GAME_OVER
}
