package com.xartifex.hundredtoone.rest;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;

import java.util.Collections;
import java.util.List;

@XmlRootElement
public class Game {

    @Inject
    private Logger log;

    private String playerName;
    private String currentQuestion;
    private long questionId;
    private String providedAnswer;
    private int lives;
    private GameState gameState = GameState.PLAYING;
    private int currentScore;

    private List<ResultRow> playerAnswers = Collections.emptyList();
    private List<ResultRow> currentMatchedAnswers = Collections.emptyList();

    private String error;

    public static final String SAME_ANSWER_ERROR = "UNIQUE_Q_P_A";
    public static final String SAME_MATCH_ERROR = "UNIQUE_Q_P_M";

    private String knownError;

    @XmlElementWrapper
    @XmlElement(name="playerAnswers")
    public List<ResultRow> getPlayerAnswers() {
        return playerAnswers;
    }

    public void setPlayerAnswers(List<ResultRow> playerAnswers) {
        this.playerAnswers = playerAnswers;
    }

    @XmlElementWrapper
    @XmlElement(name="currentMatchedAnswers")
    public List<ResultRow> getCurrentMatchedAnswers() {
        return currentMatchedAnswers;
    }

    public void setCurrentMatchedAnswers(List<ResultRow> matchedAnswers) {
        this.currentMatchedAnswers = matchedAnswers;
    }

    @NotNull
    @Size(min = 1, max = 15, message = "1-15 letters and spaces")
    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(String currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public String getProvidedAnswer() {
        return providedAnswer;
    }

    public void setProvidedAnswer(String providedAnswer) {
        this.providedAnswer = providedAnswer;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    @XmlTransient
    public Logger getLog() {
        return log;
    }

    public void setLog(Logger log) {
        this.log = log;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getKnownError() {
        return knownError;
    }

    public void setKnownError(String knownError) {
        this.knownError = knownError;
    }

    @Override
    public String toString() {
        return "Game{" +
                "playerName='" + playerName + '\'' +
                ", currentQuestion='" + currentQuestion + '\'' +
                ", questionId=" + questionId +
                ", providedAnswer='" + providedAnswer + '\'' +
                ", lives=" + lives +
                ", gameState=" + gameState +
                ", currentScore=" + currentScore +
                ", playerAnswers=" + playerAnswers +
                ", currentMatchedAnswers=" + currentMatchedAnswers +
                ", error='" + error + '\'' +
                ", knownError='" + knownError + '\'' +
                '}';
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

}
