package com.xartifex.hundredtoone.rest.admin;

import com.xartifex.hundredtoone.data.GameDAO;
import com.xartifex.hundredtoone.user.User;
import com.xartifex.hundredtoone.user.UserDAO;
import com.xartifex.hundredtoone.user.UserInfo;
import com.xartifex.hundredtoone.util.Util;
import com.xartifex.hundredtoone.model.Question;
import org.slf4j.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

@RolesAllowed({"admins"})
@Path("/admin")
@Stateless
public class AdminRest {

    @Inject
    Logger log;

    @Inject
    GameDAO gameDAO;

    @Inject
    UserDAO userDAO;

    @POST
    @Path("/addQuestions")
    @Consumes("text/plain")
    @Produces("text/plain")
    public Response addQuestions(String data) {
        List<Question> questions = Util.getQuestions(data);
        gameDAO.addQuestions(questions);
        log.info("Questions to add: " + questions);
        return Response.status(Response.Status.OK).entity("Questions added.").build();
    }

    @POST
    @Path("/addPlayers")
    @Consumes("text/plain")
    @Produces("text/plain")
    public Response addPlayers(String data) {
        Set<UserInfo> users = Util.getUsers(data);
        userDAO.addUsers(users);
        log.info("Players to add: " + users);
        return Response.status(Response.Status.OK).entity("Users added.").build();
    }
}
