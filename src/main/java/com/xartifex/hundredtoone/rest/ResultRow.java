package com.xartifex.hundredtoone.rest;

import com.xartifex.hundredtoone.model.Answer;
import com.xartifex.hundredtoone.model.PlayerAnswer;

public class ResultRow {
    private String question;
    private String answer;
    private int score;
    private Integer popularity;

    //no match case
    public ResultRow(PlayerAnswer playerAnswer)
    {
        this.question = playerAnswer.getQuestion().getQuestion();
        this.answer = playerAnswer.getAnswer();
        this.score = playerAnswer.getScore();
        this.popularity = playerAnswer.getMatch() == null ? 0 : playerAnswer.getMatch().getPopularity();
    }

    //match found case
    public ResultRow(Answer answer) {
        this.question = answer.getQuestion().getQuestion();
        this.answer = answer.getAnswer();
        this.score = answer.getScore();
        this.popularity = answer.getPopularity();
    }

    public ResultRow(String question, String answer, int score, Integer popularity) {
        this.question = question;
        this.answer = answer;
        this.score = score;
        this.popularity = popularity;
    }

    public ResultRow(){

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultRow resultRow = (ResultRow) o;

        if (score != resultRow.score) return false;
        if (question != null ? !question.equals(resultRow.question) : resultRow.question != null) return false;
        if (answer != null ? !answer.equals(resultRow.answer) : resultRow.answer != null) return false;
        return popularity != null ? popularity.equals(resultRow.popularity) : resultRow.popularity == null;
    }

    @Override
    public int hashCode() {
        int result = question != null ? question.hashCode() : 0;
        result = 31 * result + (answer != null ? answer.hashCode() : 0);
        result = 31 * result + score;
        result = 31 * result + (popularity != null ? popularity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ResultRow{" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", score=" + score +
                ", popularity=" + popularity +
                '}';
    }
}
