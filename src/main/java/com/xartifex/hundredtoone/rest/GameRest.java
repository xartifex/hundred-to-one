package com.xartifex.hundredtoone.rest;

import com.xartifex.hundredtoone.data.GameDAO;
import com.xartifex.hundredtoone.model.*;
import com.xartifex.hundredtoone.user.UserInfoProvider;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.*;
import javax.transaction.NotSupportedException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.Principal;
import java.util.*;

import org.slf4j.Logger;

import static com.xartifex.hundredtoone.data.GameDAO.INITIAL_LIVES;
import static com.xartifex.hundredtoone.rest.Game.SAME_ANSWER_ERROR;
import static com.xartifex.hundredtoone.rest.Game.SAME_MATCH_ERROR;

@RolesAllowed({"players"})
@Path("/play")
@Stateless
public class GameRest {

    public static final int MATCH_THRESHOLD = 72;

    @Inject
    private Validator validator;

    @Inject
    Logger log;

    @Inject
    GameDAO gameDAO;

    @Inject
    UserInfoProvider userInfoProvider;

    @Inject
    @NotNull
    Principal principal;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Response getGame(PlayRequest playRequest) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        String playerName = principal.getName();

        Game game = new Game();

        Response.ResponseBuilder builder = null;
        try {
            validatePlayRequest(playRequest);

            if (playerName.isEmpty()) {
                throw new IllegalStateException();
            }

            String answer = playRequest.getAnswer();

            Player player = gameDAO.findByName(playerName);
            if (player == null) {
                player = gameDAO.addNewPlayer(playerName);
            }
            if (player.getGameState() == com.xartifex.hundredtoone.model.GameState.GAME_OVER) {
                game.setGameState(GameState.GAME_OVER);
                game.setPlayerName(userInfoProvider.getUserInfoByEmail(playerName).resolveName());
                List<ResultRow> resultRows = new ArrayList<>();
                for (PlayerAnswer p : gameDAO.getPlayerAnswers(player)) {
                    resultRows.add(new ResultRow(p));
                }
                game.setPlayerAnswers(resultRows);
            } else {
                if (answer != null && !answer.isEmpty()) {
                    game.setProvidedAnswer(answer);
                    Question question = player.getCurrentQuestion();
                    Set<Answer> answers = question.getAnswers();
                    if (answers.size() == 0) {
                        throw new IllegalStateException();
                    }
                    Answer givenAnswer = new Answer();
                    givenAnswer.setAnswer(answer);
                    givenAnswer.setQuestion(question);
                    String givenAnswerStr = givenAnswer.getAnswer().toLowerCase().trim().replaceAll("," ,"");
                    boolean noMatch = true;
                    Answer matchedAnswer = null;
                    for (Answer a : answers) {
                        String currentAnswerStr = a.getAnswer().toLowerCase().trim().replaceAll(",", "");
                        //todo: also somehow handle case where there is more then one match
                        boolean synonymFound = false;
                        Set<Synonym> synonyms = a.getSynonyms();
                        for (Synonym synonym : synonyms) {
                            if (FuzzySearch.ratio(synonym.getSynonym().toLowerCase().trim(), givenAnswerStr) >= MATCH_THRESHOLD) {
                                synonymFound = true;
                            }
                        }
                        if (synonymFound || FuzzySearch.ratio(currentAnswerStr, givenAnswerStr) >= MATCH_THRESHOLD) {
                            givenAnswer.setScore(a.getScore());
                            givenAnswer.setPopularity(a.getPopularity());
                            noMatch = false;
                            matchedAnswer = a;
                            break;
                        }
                    }
                    if (noMatch) {
                        givenAnswer.setPopularity(0);
                        givenAnswer.setScore(0);
                        player.setLives(player.getLives() - 1);

                        PlayerStatePK playerStatePK = new PlayerStatePK();
                        playerStatePK.setPlayer(player);
                        playerStatePK.setQuestion(player.getCurrentQuestion());
                        PlayerState playersState = new PlayerState();
                        playersState.setPlayerStatePK(playerStatePK);
                        playersState.setLives(player.getLives());
                        gameDAO.updatePlayersState(playersState);
                    }


                    PlayerAnswer playerAnswer = new PlayerAnswer();
                    playerAnswer.setQuestion(player.getCurrentQuestion());
                    playerAnswer.setPlayer(player);
                    playerAnswer.setMatch(matchedAnswer);
                    playerAnswer.setScore(givenAnswer.getScore());
                    playerAnswer.setAnswer(givenAnswer.getAnswer());
                    gameDAO.addPlayersAnswer(playerAnswer);

                    //
                  /*
                        assume that:
                        1) PlayerAnswer table cannot contain the same match for several answers
                        2) All answers are unique
                        these two conditions are enforced by annotations (see game7 and game9)
                    */
                    //todo: avoid calling it two times
                    Set<Answer> matchedAnswers = gameDAO.getCurrentMatchedAnswers(player);
                    //

                    //this is a quick bugfix for case when player found all answers for a question without an error
                    if (matchedAnswers.size() == question.getAnswers().size()) {
                        PlayerStatePK playerStatePK = new PlayerStatePK();
                        playerStatePK.setPlayer(player);
                        playerStatePK.setQuestion(player.getCurrentQuestion());
                        PlayerState playersState = new PlayerState();
                        playersState.setPlayerStatePK(playerStatePK);
                        playersState.setLives(player.getLives());
                        gameDAO.updatePlayersState(playersState);
                    }

                    if (player.getLives() == 0 || (matchedAnswers.size() == question.getAnswers().size())) {
                        Question nextQuestion = gameDAO.getNextQuestion(player);
                        if (nextQuestion == null) {
                            List<ResultRow> resultRows = new ArrayList<>();
                            for (PlayerAnswer p : gameDAO.getPlayerAnswers(player)) {
                                resultRows.add(new ResultRow(p));
                            }
                            game.setPlayerAnswers(resultRows);
                            game.setGameState(GameState.GAME_OVER);
                            player.setGameState(com.xartifex.hundredtoone.model.GameState.GAME_OVER);
                        } else {
                            player.setLives(INITIAL_LIVES);
                            player.setCurrentQuestion(nextQuestion);
                        }
                    }
                }

                List<ResultRow> matchedAnswers = new ArrayList<>();
                for (Answer matchedAnswer : gameDAO.getCurrentMatchedAnswers(player)) {
                    matchedAnswers.add(new ResultRow(matchedAnswer));
                }
                game.setCurrentMatchedAnswers(matchedAnswers);
                game.setPlayerName(userInfoProvider.getUserInfoByEmail(playerName).resolveName());
                game.setCurrentQuestion(game.getGameState() == GameState.PLAYING
                        ? player.getCurrentQuestion().getQuestion() : "");
                game.setQuestionId(player.getCurrentQuestion().getQuestionId());
                game.setLives(player.getLives());
                game.setCurrentScore(gameDAO.getCurrentScore(player));
            }
            builder = Response.ok().entity(game);
        }
        //todo refactor this: get rid of dependency on org.hibernate.exception
        catch (PersistenceException e) {
            if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
                org.hibernate.exception.ConstraintViolationException error =
                        (org.hibernate.exception.ConstraintViolationException) e.getCause();
                if (error.getConstraintName().contains(SAME_ANSWER_ERROR)) {
                    game.setKnownError(SAME_ANSWER_ERROR);
                } else if (error.getConstraintName().contains(SAME_MATCH_ERROR)) {
                    game.setKnownError(SAME_MATCH_ERROR);
                }
                builder = Response.status(Response.Status.BAD_REQUEST).entity(game);
            } else {
                log.error("Error during /play invocation: ", e);
                Map<String, String> responseObj = new HashMap<>();
                responseObj.put("error", e.getMessage());
                builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
            }
        } catch (ConstraintViolationException ce) {
            Map<String, String> responseObj = new HashMap<>();
            for (ConstraintViolation<?> violation : ce.getConstraintViolations()) {
                responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
            }
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        } catch (Exception e) {
            log.error("Error during /play invocation: ", e);
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    private void validatePlayRequest(final PlayRequest playRequest) {
        // Create a bean validator and check for issues.
        Set<ConstraintViolation<PlayRequest>> violations = validator.validate(playRequest);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }
    }
}
