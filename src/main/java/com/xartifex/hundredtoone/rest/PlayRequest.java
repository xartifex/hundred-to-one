package com.xartifex.hundredtoone.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PlayRequest {
    public PlayRequest() {

    }

    public PlayRequest(String answer) {
        this.answer = answer;
    }

    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "PlayRequest{" +
                "answer='" + answer + '\'' +
                '}';
    }
}
