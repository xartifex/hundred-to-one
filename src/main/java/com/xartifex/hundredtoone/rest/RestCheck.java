package com.xartifex.hundredtoone.rest;

import com.xartifex.hundredtoone.model.*;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.transaction.*;
import javax.transaction.NotSupportedException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

import static com.xartifex.hundredtoone.data.GameDAO.INITIAL_LIVES;

@RolesAllowed({"players"})
@Path("/check")
//@Stateless
public class RestCheck {

    @GET
    @Consumes({ "application/json" })
    @Produces(MediaType.TEXT_PLAIN)
    public Response getGame(PlayRequest playRequest) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        Response.ResponseBuilder builder = null;
        builder = Response.ok().entity("Passed!");
        return builder.build();
    }

}
